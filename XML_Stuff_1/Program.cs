﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;

namespace XML_Stuff_1
{
    class Program
    {
        static void Main(string[] args)
        {

            //lets read the xml
            lets_read_xml_1();

            //lets write an xml and return it
            string written_xml = lets_write_xml_1();

            Console.WriteLine("------------------------------GENERATE XML STARTS HERE-------------------------------------------");
            Console.WriteLine(written_xml);
            Console.WriteLine("------------------------------GENERATE XML ENDS HERE-------------------------------------------");

            //this will add some xml stuff and also show xml using xmldocument
            lets_do_some_xml_1();


            Console.WriteLine("we can give you 62 men");
            Console.WriteLine("That's all folks!");

            Console.ReadLine();
        }


        //this is where I will do same xml modifications as I did in 
        //lets_read_xml_1 and lets_write_xml_1
        //but using the much better xmldocument class
        private static void lets_do_some_xml_1()
        {
            //getting the XMLDocument object
            XmlDocument object_xml_1 = new XmlDocument();

            //getting the xml for processsing
            string xml_for_processing = return_xml_1();

            //we should load the xml string first
            object_xml_1.LoadXml(xml_for_processing);

            //let me get all the nodes with the person tag
            //in my xml, I have two person 
            XmlNodeList list_of_nodes = object_xml_1.GetElementsByTagName("person");

            //the above list has all the nodes of the tag with person
            //now I can just traverse the list and display what I want.
            foreach(XmlNode temp_xml_node in list_of_nodes)
            {
                string first_name = temp_xml_node.Attributes["firstname"].Value;
                string last_name = temp_xml_node.Attributes["lastname"].Value;
                string email_adddress = temp_xml_node.InnerText;

                Console.WriteLine("full name - {0} {1} email address - {2}", first_name, last_name,email_adddress);
            }

            //display of names is done. 
            //now lets add some nodes of our own
            //adding another person called blog nildana with email address blog@studynildana.com

            //lets create a new node
            //I am specifying that this new node will be an element
            //with the tag person, just like the tags we used above
            //the last thing is blank because in the sample xml we are not using a URI
            XmlNode node_new = object_xml_1.CreateNode(XmlNodeType.Element, "person", "");

            //let me set the attribute first name and last name
            XmlAttribute first_name_attribute = object_xml_1.CreateAttribute("firstname");
            first_name_attribute.Value = "blog";
            XmlAttribute last_name_attribute = object_xml_1.CreateAttribute("lastname");
            last_name_attribute.Value = "nildana";

            //now adding these attributes to the new node
            node_new.Attributes.Append(first_name_attribute);
            node_new.Attributes.Append(last_name_attribute);

            //okay, let me add an email address as well

            //now creating a node with contact details which goes inside the new node
            XmlNode node_contact_details = object_xml_1.CreateNode(XmlNodeType.Element, "contactdetails", "");
            //creating a node with email address which goes inside contact details
            XmlNode node_email_address = object_xml_1.CreateNode(XmlNodeType.Element, "emailaddress", "");
            node_email_address.InnerText = "blog@studynildana.com";
            //adding email address as a child to contact details
            node_contact_details.AppendChild(node_email_address);
            //adding contact details to new node
            node_new.AppendChild(node_contact_details);

            //now the node is ready. adding it to the above xml object
            object_xml_1.DocumentElement.AppendChild(node_new);

            //now saving to a stream
            StringWriter temp_string_stream = new StringWriter();

            object_xml_1.Save(temp_string_stream);

            xml_for_processing = temp_string_stream.ToString();

            //alright, now lets display the xml file again, and it should show the extra name

            //we should load the xml string first
            object_xml_1.LoadXml(xml_for_processing);

            //let me get all the nodes with the person tag
            //in my xml, I have two person 
            list_of_nodes = object_xml_1.GetElementsByTagName("person");

            //the above list has all the nodes of the tag with person
            //now I can just traverse the list and display what I want.

            Console.WriteLine("This list shows the two from before and also the new one we just added");
            foreach (XmlNode temp_xml_node in list_of_nodes)
            {
                string first_name = temp_xml_node.Attributes["firstname"].Value;
                string last_name = temp_xml_node.Attributes["lastname"].Value;
                string email_adddress = temp_xml_node.InnerText;

                Console.WriteLine("full name - {0} {1} email address - {2}", first_name, last_name, email_adddress);
            }

            //now lets search for a specific person from the xml file
            Console.WriteLine("Enter the person you wish to search for");
            string person_to_search = Console.ReadLine();

            //as before, an xmldocument object
            XmlDocument search_xml_document_object = new XmlDocument();
            search_xml_document_object.LoadXml(xml_for_processing);

            //first we will need a xml navigator 
            XPathNavigator navigator_xml_object = search_xml_document_object.CreateNavigator();

            //next build our query
            string query_for_person = "//people/person[@firstname='" + person_to_search + "']";

            //have the navigator run the query string
            //collect the results in an node iterator object
            XPathNodeIterator person_iterator = navigator_xml_object.Select(query_for_person);

            Console.WriteLine("Here are the {0} persons we found ", person_iterator.Count);
            //the results are now contained in the iterator object. lets display them
            while (person_iterator.MoveNext())
            {
                string first_name = person_iterator.Current.GetAttribute("firstname", "");
                string last_name = person_iterator.Current.GetAttribute("lastname", "");

                Console.WriteLine("name found is {0} {1}", first_name, last_name);
            }

        }



        private static string lets_write_xml_1()
        {
            string xml_string_to_return = "";
            //first I will need a string stream object

            StringWriter stream_object_stream = new StringWriter();

            //now I need a xmlwriter stream object and start building the xml file

            //so, I am creating the xml writer object which connects with the string stream that will hold the actual stream
            //further, i am setting that the Indent property is true.
            //this means, new items will be written in new line, with proper tab spacing like how this code is written
            //it makes the XML file more human readable.
            using (XmlWriter stream_object_xml_writer = XmlWriter.Create(stream_object_stream, new XmlWriterSettings() { Indent = true }))
            {
                //first need to start the document
                stream_object_xml_writer.WriteStartDocument();

                //while building the xml document, you will notice how the methods we are calling
                //mirror the xml file that is created directly in the method 
                //which means, it is upon me, the developer to start as well as end elements that are created.
                //then, lets start individual person elements 
                
                //opening the people element
                stream_object_xml_writer.WriteStartElement("people");
                //opening the person element
                stream_object_xml_writer.WriteStartElement("person");

                //the first set, first and last name
                //these are being addded as attributes, hence the attribute write is being used
                stream_object_xml_writer.WriteAttributeString("firstname", "study");
                stream_object_xml_writer.WriteAttributeString("secondname", "nildana");

                //opening the contact details element
                stream_object_xml_writer.WriteStartElement("contactdetails");
                //adding the email as a value unlike the attribute we did above
                stream_object_xml_writer.WriteElementString("emailaddress", "support@studynildana.com");

                //closing the contact details element
                stream_object_xml_writer.WriteEndElement();
                //closing the person element
                stream_object_xml_writer.WriteEndElement();

                //note that I am not closing the people element
                //this is because, it is the root element, and does not need manual closing
                //when the writer finishes its work, it will automatically do the closing of the root node
                //which is people
                stream_object_xml_writer.Flush();

                
            }
            xml_string_to_return = stream_object_stream.ToString();

                //return our generated xml string
                return xml_string_to_return;
        }

        //this method will use the XmlReader
        private static void lets_read_xml_1()
        {
            //getting the xml string for processing
            string xml_stuff = return_xml_1();

            //getting a stream object to read the actual string
            using (StringReader string_reading_object = new StringReader(xml_stuff))
            {
                //now turning that into an xmlreader stream object
                //the xmlreader constructor insists on having a settings parameter as well
                //I am going to ignore whitespace because the XML file will probably have lots of white space
                using (XmlReader xml_reading_object = XmlReader.Create(string_reading_object, new XmlReaderSettings() { IgnoreWhitespace = true }))
                {
                    //now I have access to the xml stream, I can do some navigation and read the contents

                    //first, lets move to where the content is actually present
                    xml_reading_object.MoveToContent();

                    //next, lets set the starting point
                    //people is the root node in the xml file, that is where I am starting
                    xml_reading_object.ReadStartElement("people");

                    //now, reading the very first person's details
                    //I am using the GetAttribute method becuase the values are set as XAML attributes
                    //first name
                    string first_name = xml_reading_object.GetAttribute("firstname");
                    //last name
                    string last_name = xml_reading_object.GetAttribute("lastname");
                    //email address
                    //to get the email address, I have to set the root node again
                    //the new root node would be 'person' so first moving to person
                    xml_reading_object.ReadStartElement("person");
                    //then again, the new root node would be 'contactdetails' so now moving to contactdetails
                    xml_reading_object.ReadStartElement("contactdetails");
                    //I am using readstring method becuase the value is set as an actual value
                    string email_address = xml_reading_object.ReadElementContentAsString();

                    //now i have the first name, last name and the email address
                    Console.WriteLine("name - {0} {1} email address - {2}", first_name, last_name, email_address);
                }
            }
        }


        //in this method, I am returning a simple xml file. 
        //since this whole solution is about playing around with xml files
        //this would be better
        static string return_xml_1()
        {
            string temp_xml = "";

            temp_xml = @"<?xml version=""1.0"" encoding=""utf-8""?>
                        <people>
                            <person firstname=""study"" lastname=""nildana"">
                                <contactdetails>
                                    <emailaddress>support@studynildana.com</emailaddress>
                                </contactdetails>
                            </person>
                            <person firstname=""study"" lastname=""nildana2"">
                                <contactdetails>
                                    <emailaddress>support@studynildana.com</emailaddress>
                                    <phonenumber>5551234567</phonenumber>
                                </contactdetails>
                            </person>    
                        </people>";

            return (temp_xml);
        }
    }
}
